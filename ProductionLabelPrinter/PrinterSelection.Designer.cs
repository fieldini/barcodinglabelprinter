﻿namespace ProductionLabelPrinter
{
    partial class PrinterSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrinterSelection));
            this.lblPrinters = new System.Windows.Forms.Label();
            this.cboPrinterListInner = new System.Windows.Forms.ComboBox();
            this.btnSettingsCancel = new System.Windows.Forms.Button();
            this.btnSettingsSave = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cboPrinterListOuter = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblPrinters
            // 
            this.lblPrinters.AutoSize = true;
            this.lblPrinters.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrinters.Location = new System.Drawing.Point(8, 16);
            this.lblPrinters.Name = "lblPrinters";
            this.lblPrinters.Size = new System.Drawing.Size(235, 18);
            this.lblPrinters.TabIndex = 7;
            this.lblPrinters.Text = "Select INNER label printer from list";
            // 
            // cboPrinterListInner
            // 
            this.cboPrinterListInner.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPrinterListInner.FormattingEnabled = true;
            this.cboPrinterListInner.Location = new System.Drawing.Point(8, 40);
            this.cboPrinterListInner.Name = "cboPrinterListInner";
            this.cboPrinterListInner.Size = new System.Drawing.Size(272, 24);
            this.cboPrinterListInner.TabIndex = 6;
            // 
            // btnSettingsCancel
            // 
            this.btnSettingsCancel.Location = new System.Drawing.Point(184, 176);
            this.btnSettingsCancel.Name = "btnSettingsCancel";
            this.btnSettingsCancel.Size = new System.Drawing.Size(75, 23);
            this.btnSettingsCancel.TabIndex = 5;
            this.btnSettingsCancel.Text = "Cancel";
            this.btnSettingsCancel.UseVisualStyleBackColor = true;
            this.btnSettingsCancel.Click += new System.EventHandler(this.btnSettingsCancel_Click);
            // 
            // btnSettingsSave
            // 
            this.btnSettingsSave.Location = new System.Drawing.Point(24, 176);
            this.btnSettingsSave.Name = "btnSettingsSave";
            this.btnSettingsSave.Size = new System.Drawing.Size(75, 23);
            this.btnSettingsSave.TabIndex = 4;
            this.btnSettingsSave.Text = "OK";
            this.btnSettingsSave.UseVisualStyleBackColor = true;
            this.btnSettingsSave.Click += new System.EventHandler(this.btnSettingsSave_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(242, 18);
            this.label1.TabIndex = 9;
            this.label1.Text = "Select OUTER label printer from list";
            // 
            // cboPrinterListOuter
            // 
            this.cboPrinterListOuter.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPrinterListOuter.FormattingEnabled = true;
            this.cboPrinterListOuter.Location = new System.Drawing.Point(9, 110);
            this.cboPrinterListOuter.Name = "cboPrinterListOuter";
            this.cboPrinterListOuter.Size = new System.Drawing.Size(272, 24);
            this.cboPrinterListOuter.TabIndex = 8;
            // 
            // PrinterSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(290, 220);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboPrinterListOuter);
            this.Controls.Add(this.lblPrinters);
            this.Controls.Add(this.cboPrinterListInner);
            this.Controls.Add(this.btnSettingsCancel);
            this.Controls.Add(this.btnSettingsSave);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PrinterSelection";
            this.Text = "PrinterSelection";
            this.Load += new System.EventHandler(this.PrinterSelection_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPrinters;
        private System.Windows.Forms.ComboBox cboPrinterListInner;
        private System.Windows.Forms.Button btnSettingsCancel;
        private System.Windows.Forms.Button btnSettingsSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboPrinterListOuter;
    }
}