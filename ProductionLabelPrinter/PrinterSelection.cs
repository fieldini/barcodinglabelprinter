﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace ProductionLabelPrinter
{
    public partial class PrinterSelection : Form
    {

        string PrinterFromMainInner;
        string PrinterFromMainOuter;

        public delegate void PrinterUpdateHandler(object sender, PrinterUpdateEventArgs e);

        public event PrinterUpdateHandler PrinterUpdated;

        public PrinterSelection(string printerMainInner, string printerMainOuter)
        {
            InitializeComponent();
            PrinterFromMainInner = printerMainInner;
            PrinterFromMainOuter = printerMainOuter;
        }

        private void PrinterSelection_Load(object sender, EventArgs e)
        {
            //populate cbo with list of installed printers
            foreach (String strPrinter in PrinterSettings.InstalledPrinters)
            {
                cboPrinterListInner.Items.Add(strPrinter);
                cboPrinterListOuter.Items.Add(strPrinter);
                //make sure current selected printer shows in the list
                if (strPrinter == PrinterFromMainInner)
                {
                    cboPrinterListInner.SelectedIndex = cboPrinterListInner.Items.IndexOf(strPrinter);
                }
                if (strPrinter == PrinterFromMainOuter)
                {
                    cboPrinterListOuter.SelectedIndex = cboPrinterListOuter.Items.IndexOf(strPrinter);
                }
            }
        }

        private void btnSettingsSave_Click(object sender, EventArgs e)
        {
            string sPrinterInner = cboPrinterListInner.Text;
            string sPrinterOuter = cboPrinterListOuter.Text;


            // instance the event args and pass it each value

            PrinterUpdateEventArgs args = new PrinterUpdateEventArgs(sPrinterInner, sPrinterOuter);

            // raise the event with the updated arguments

            PrinterUpdated(this, args);

            this.Dispose();
        }



        private void btnSettingsCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }





    }

    public class PrinterUpdateEventArgs : System.EventArgs
    {
        // add local member variables to hold text
        private string mPrinterInner;
        private string mPrinterOuter;

        // class constructor
        public PrinterUpdateEventArgs(string sPrinterInner, string sPrinterOuter)
        {
            this.mPrinterInner = sPrinterInner;
            this.mPrinterOuter = sPrinterOuter;

        }

        // Properties - Viewable by each listener
        public string PrinterInner
        {
            get
            {
                return mPrinterInner;
            }
        }

        public string PrinterOuter
        {
            get
            {
                return mPrinterOuter;
            }
        }



    }
}
