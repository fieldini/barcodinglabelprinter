﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using System.IO.IsolatedStorage;
using System.IO;
using System.Xml;
using System.Threading;
using System.Threading.Tasks;

namespace ProductionLabelPrinter
{
    public partial class Form1 : Form
    {

        private Splash splashScreen;
        private bool done = false;
        InnerLabels inner_rpt;
        OuterLabels outer_rpt;
        blank_report blank_rpt;

        public Form1()
        {
            InitializeComponent();
            this.splashScreen = new Splash();
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Hide();

            Thread thread = new Thread(new ThreadStart(this.ShowSplashScreen));
            thread.Start();
            load_settings();

            //this is runned to preload crystal assemblies on a seperate thread
            var crPreload = Task.Factory.StartNew(() =>
            {
                
                ReportDocument preloadCrystalReport = new ReportDocument();
                blank_rpt = new blank_report();
                preloadCrystalReport = blank_rpt;
                PrintPreview preloadCrystalGUI = new PrintPreview();
                preloadCrystalGUI.Init(preloadCrystalReport);//    .Init(preloadCrystalReport);
                preloadCrystalGUI.Dispose();
                //begin preloading crystal report rpt files
                preloadCrystalReports();


            }, TaskCreationOptions.LongRunning);
            crPreload.Wait();
            done = true;
            this.Show();

        }

        private void ShowSplashScreen()
        {
            splashScreen.Show();
            splashScreen.Activate();
            while (!done)
            {
                Application.DoEvents();
            }
            splashScreen.Close();
            this.splashScreen.Dispose();
        }

        private void preloadCrystalReports()
        {
            inner_rpt = new InnerLabels();
            inner_rpt.Load();

            outer_rpt = new OuterLabels();
            outer_rpt.Load();

        }


        //test

        //SqlConnection conn = new SqlConnection("server=serversbo01;UID=sa;password=B1Admin;database=Rockdoortest");
        //string gdbname = "rockdoortest";
        //live

        SqlConnection conn = new SqlConnection("server=serversbo01;UID=sa;password=B1Admin;database=Rockdoorlive");
        string gdbname = "rockdoorlive";

        string printerInner;
        string printerOuter;
        string InOrOut;
        
        
        

        private void load_settings()
        {
            //This procedure runs when the form loads. When the program is run
            //It loads up the xml file with the settings.

            IsolatedStorageFile isoStorage;
            isoStorage = IsolatedStorageFile.GetUserStoreForDomain();

            Array StoreFileNames;
            // String  StoreFile;
            StoreFileNames = isoStorage.GetFileNames("settings.xml");


            foreach (string StoreFile in StoreFileNames)
            {
                if (StoreFile == "settings.xml")
                {
                    try
                    {
                        using (StreamReader stmReader = new StreamReader((new IsolatedStorageFileStream("settings.xml", FileMode.Open, isoStorage))))
                        {
                            XmlTextReader xmlReader = new XmlTextReader(stmReader);

                            while (xmlReader.Read())
                            {
                                if (xmlReader.NodeType == XmlNodeType.Element)
                                {
                                    if (xmlReader.Name == "printerIn")
                                    {
                                        printerInner = xmlReader.ReadString();
                                    }
                                    if (xmlReader.Name == "printerOut")
                                    {
                                        printerOuter = xmlReader.ReadString();
                                    }

                                }
                            }
                            xmlReader.Close();
                            stmReader.Close();
                        }


                    }


                    catch (System.IO.IOException)
                    {
                        MessageBoxButtons buttons = MessageBoxButtons.OK;
                        DialogResult result;

                        result = MessageBox.Show("Instance of this program already open", "Click OK to close", buttons);

                        if (result == System.Windows.Forms.DialogResult.OK)
                        {
                            this.Close();
                        }


                    }


                }
            }
            isoStorage.Close();
        }


        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            //this procedure saves the settings for the program

            IsolatedStorageFile isoStorage;
            isoStorage = IsolatedStorageFile.GetUserStoreForDomain();

            using (IsolatedStorageFileStream stmWriter = (new IsolatedStorageFileStream("settings.xml", FileMode.Create, isoStorage)))
            {
                XmlDocument Doc = new XmlDocument(); //define new xml document
                XmlDeclaration myXMLdeclaration; //declare xml settings
                myXMLdeclaration = Doc.CreateXmlDeclaration("1.0", "UTF-8", "yes");  //pass xml settings to document
                Doc.AppendChild(myXMLdeclaration);
                //create root node or element for document
                XmlElement Root;
                Root = Doc.CreateElement("Settings"); //root node is called settings
                XmlElement printerIn;
                printerIn = Doc.CreateElement("printerIn"); //inside the root node is the child element for tab
                printerIn.InnerText = printerInner; //set the text for this node to be stage selected from stage variable
                XmlElement printerOut;
                printerOut = Doc.CreateElement("printerOut"); //inside the root node is the child element for tab
                printerOut.InnerText = printerOuter; //set the text for this node to be stage selected from stage variable     

                Root.AppendChild(printerIn); //add child to document
                Root.AppendChild(printerOut); //add child to document
                Doc.AppendChild(Root); //add root to document
                try
                {
                    XmlTextWriter output = new XmlTextWriter(stmWriter, System.Text.Encoding.UTF8);
                    Doc.WriteTo(output); //use xml txt writer to write out the Doc
                    //MessageBox.Show("Settings Saved!");
                    output.Close(); //close the document
                }

                catch (System.Exception exp)
                {
                    if (exp is System.IO.IOException || exp is System.UnauthorizedAccessException)
                    {
                        MessageBox.Show("Settings save failed. Contact Administrator");
                    }
                    else
                    {
                        throw;
                    }
                }

                stmWriter.Close();
                isoStorage.Close();
            }
        }
        

        private void choosePrinterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //pass current selected printer through to the form
            PrinterSelection SetForm = new PrinterSelection(printerInner, printerOuter);

            SetForm.PrinterUpdated += new PrinterSelection.PrinterUpdateHandler(PrinterSelection_ButtonClicked);

            SetForm.Show();
        }
        private void PrinterSelection_ButtonClicked(object sender, PrinterUpdateEventArgs e)
        {
            // update the forms values from the event args
            printerInner = e.PrinterInner;
            printerOuter = e.PrinterOuter;

        }

        private void btnInner_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            //make label highlight as selected
            btnInner.BackColor = Color.DodgerBlue;
            btnOuter.BackColor = Color.White;
            //change list to inner label list
            InOrOut = "Inner";
            FillDGV();
            this.Cursor = Cursors.Default;
        }

        private void btnOuter_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            //make label highlight as selected
            btnOuter.BackColor = Color.DodgerBlue;
            btnInner.BackColor = Color.White;
            //change list to inner label list
            InOrOut = "Outer";
            FillDGV();
            this.Cursor = Cursors.Default;
        }

        private void FillDGV()
        {
            //fill datagrid with list of labels to print off

            
            SqlDataAdapter myDataAdapterDels = new SqlDataAdapter();

            SqlCommand cmd;
            cmd = new SqlCommand("sp_BarCode_GetLabels", conn);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter prmInOrOut = new SqlParameter("@InOrOut", SqlDbType.NVarChar, 80);
            prmInOrOut.Direction = ParameterDirection.Input;
            prmInOrOut.Value = InOrOut;

            cmd.Parameters.Add(prmInOrOut);

            try
            {
                conn.Open();
                            
                myDataAdapterDels.SelectCommand = cmd;
                
                DataTable t = new DataTable();
                myDataAdapterDels.Fill(t);

                dgvList.DataSource = t;
                dgvList.DefaultCellStyle.SelectionBackColor = Color.Orange;
                dgvList.Columns["Job Revision"].Visible = false;


            }

            finally
            {

                conn.Close();

            }


        }



        private void btnPrint_Click(object sender, EventArgs e)
        {
            List<string> JobNumbers = new List<string>();
            List<string> JobNumbersRev = new List<string>();


            DialogResult result1 = MessageBox.Show("Do you want to print these selections?","Question",  MessageBoxButtons.YesNo);

            if (result1 == DialogResult.Yes)
            {
                this.Cursor = Cursors.WaitCursor;

                Int32 selectedRowCount = dgvList.Rows.GetRowCount(DataGridViewElementStates.Selected);
                                
                if (selectedRowCount != 0)
                {

                    foreach (DataGridViewRow row in this.dgvList.SelectedRows)
                    {
                        DataGridViewCell cell = row.Cells[3]; //job numbers with revision for log
                        DataGridViewCell cellj = row.Cells[0]; //job numbers only for printing

                        JobNumbers.Add(cell.Value.ToString());
                        JobNumbersRev.Add(cellj.Value.ToString());
                    }

                    PrintCrystalReport(JobNumbersRev);
                }



            }

            this.Cursor = Cursors.Default;


            DialogResult result2 = MessageBox.Show("Did the labels print off successfully?", "Question", MessageBoxButtons.YesNo);
            //if labels printed ok then update table with list of printed jobs. 
            if (result2 == DialogResult.Yes)
            {
                //convert list to array
                string[] j = JobNumbers.ToArray();
                //pass array to XML builder
                LabelLog(BuildXmlString("JobNo",j));
            }
            //update dgv
            FillDGV();
       }


        void PrintCrystalReport(List<string> JobNumbers)
        {
            //print off the CNC label/Despatch label on the selected printer.
            //show crystal report code



            ReportDocument delmanCR = new ReportDocument();


            switch (InOrOut)
            {
                case "Inner":
                    delmanCR = inner_rpt;
                    break;
                case "Outer":
                    delmanCR = outer_rpt;
                    break;
            }


            ConnectionInfo MyConnectionInfo = new ConnectionInfo();

            MyConnectionInfo.ServerName = "serversbo01";
            MyConnectionInfo.DatabaseName = gdbname;
            MyConnectionInfo.UserID = "sa";
            MyConnectionInfo.Password = "B1Admin";

            Tables myTables = delmanCR.Database.Tables;


            foreach (CrystalDecisions.CrystalReports.Engine.Table myTable in myTables)
            {
                TableLogOnInfo myTableLogonInfo = myTable.LogOnInfo;
                myTableLogonInfo.ConnectionInfo = MyConnectionInfo;
                myTable.ApplyLogOnInfo(myTableLogonInfo);
            }

            Sections mySections = delmanCR.ReportDefinition.Sections;

            foreach (Section mySection in mySections)
            {
                ReportObjects myReportObjects = mySection.ReportObjects;

                foreach (ReportObject myReportObject in myReportObjects)
                {
                    if (myReportObject.Kind == ReportObjectKind.SubreportObject)
                    {
                        SubreportObject mySubreportObject = (SubreportObject)myReportObject;
                        ReportDocument subReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName);
                    }
                }
            }


            string[] j = JobNumbers.ToArray();

            delmanCR.SetParameterValue("JobNo", j);

            switch (InOrOut)
            {
                case "Inner":
                    delmanCR.PrintOptions.PrinterName = printerInner;
                    break;
                case "Outer":
                    delmanCR.PrintOptions.PrinterName = printerOuter;
                    delmanCR.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
                    break;
            }
            
            delmanCR.PrintToPrinter(1, false, 0, 0);


        }


        public static string BuildXmlString(string xmlRootName, string[] values)
            //build an xml string for sql to use for insert
        {
            StringBuilder xmlString = new StringBuilder();

            xmlString.AppendFormat("<{0}>", xmlRootName);
            for (int i = 0; i < values.Length; i++)
            {
                xmlString.AppendFormat("<value>{0}</value>", values[i]);
            }
            xmlString.AppendFormat("</{0}>", xmlRootName);

            return xmlString.ToString();
        }



        public void LabelLog(string XML)
        {
            //log of labels that have been printed
            SqlCommand cmd = new SqlCommand("sp_BarCode_LabelInsert", conn);

            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter prmJobNo = new SqlParameter("@JobNo", SqlDbType.Xml);
            prmJobNo.Direction = ParameterDirection.Input;
            prmJobNo.Value = XML;

            SqlParameter prmInOrOut = new SqlParameter("@InOrOut", SqlDbType.NVarChar, 20);
            prmJobNo.Direction = ParameterDirection.Input;
            prmInOrOut.Value = InOrOut;

            cmd.Parameters.Add(prmJobNo);
            cmd.Parameters.Add(prmInOrOut);

            try
            {

                conn.Open();

                cmd.ExecuteNonQuery();

                
            }

            finally
            {

                conn.Close();

            }
        }

        private void dgvList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

                DataGridViewRow dgvRow = dgvList.Rows[e.RowIndex];
                if (dgvRow.Cells[3].Value.ToString().IndexOf("R") != -1)
                {
                    dgvRow.DefaultCellStyle.BackColor = Color.Tomato;
                }
                else
                {
                    dgvRow.DefaultCellStyle.BackColor = Color.White;
                }

        }



    }
}
